import 'package:flutter/material.dart';
//import 'package:forecanvass/codeInput.dart';

//import 'text.dart';
//import 'usernamesAndPasswords.dart';

class UserLogin extends StatefulWidget {
  UserLogin({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _UserLoginState createState() => _UserLoginState();
}

class _UserLoginState extends State<UserLogin> {
  // var allText = AllText();

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();

  //Controllers to get values from textfields later.
  final userNameController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    userNameController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
          //  title: Text(allText.loginText),
          ),
      body: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            TextFormField(
              controller: userNameController,
              decoration: InputDecoration(labelText: 'Username'),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Invalid Username';
                }
              },
            ),
            TextFormField(
              controller: passwordController,
              decoration: InputDecoration(labelText: 'Password'),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Invalid Password';
                }
              },
            ),
            RaisedButton(
              onPressed: () {
                /*  if (_formKey.currentState.validate()) {
                  if (loginValidation(
                      userNameController.text, passwordController.text)) {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (context) => CodeInput()),
                    );
                  } else {
                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                        content: Text('Invalid Username or Password')));
                  }
                } */
              },
              child: Text('Login'),
            )
          ],
        ),
      ),
    );
  }
}

/* bool loginValidation(String userName, String password) {
  for (int i = 0; i < users.length; i++) {
    if (users[i].userName == userName && users[i].password == password) {
      return true;
    }
  }
  return false; 
}*/
