import 'dart:async';
import 'package:sqflite/sqflite.dart';
import '../models/logIn.dart';
import 'package:path/path.dart';

class DBProvider2 {
  String customerTable = 'Customer_table';

  DBProvider2._();

  static final DBProvider2 db = DBProvider2._();
  static Database _database;

  // Checks if database exists.
  Future<Database> get database async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  // Creates database if one isn't on the device already. Will not create a new database
  // if one exists already.
  initDB() async {
    //  Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String databasesPath = await getDatabasesPath();
    //String path = join(documentsDirectory.path, "TestDB.db");
    String path = join(databasesPath, "TestDB.db");

    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE Profile ("
          "id INTEGER PRIMARY KEY,"
          "first_name TEXT,"
          "last_name TEXT,"
          "email TEXT,"
          "FOREIGN KEY (id) REFERENCES families(Customer)" //??
          ")");
    });
  }

  Future<int> createCustomer(Customer customer) async {
//var result = await _database.insert("Customer", customer.toJson());
//var result = await initDB().insert("Customer", customer.toJson());
    final db = await database; //??
    var result = await db.insert("Customer", customer.toJson());
    return result;
    // Method to get a list of people with the same temporary code.
  }

  Future<int> createCustomer1(Customer customer) async {
    final db = await database; //??
    var result = await db.rawInsert(
        "INSERT INTO Customer (id,first_name, last_name, email)"
        " VALUES (${customer.id},${customer.firstName},${customer.lastName},${customer.email})");
    return result;
  }

  Future<List> getCustomers() async {
    Database db = await this.database; //??
    var result = await db
        .query("Customer", columns: ["id", "first_name", "last_name", "email"]);

    return result.toList();
  }

  Future<List> getCustomers1() async {
    final db = await database; //??
    var result = await db.rawQuery('SELECT * FROM Customer');
    return result.toList();
  }

  Future<Customer> getCustomer(int id) async {
    final db = await database; //??

    List<Map> results = await db.query("Customer",
        columns: ["id", "first_name", "last_name", "email"],
        where: 'id = ?',
        whereArgs: [id]);

    if (results.length > 0) {
      return new Customer.fromJson(results.first);
    }

    return null;
  }

  Future<Customer> getCustomer1(int id) async {
    final db = await database; //??
    var results = await db.rawQuery('SELECT * FROM Customer WHERE id = $id');

    if (results.length > 0) {
      return new Customer.fromJson(results.first);
    }

    return null;
  }

  Future<int> updateCustomer(Customer customer) async {
    final db = await database; //??

    return await db.update("Customer", customer.toJson(),
        where: "id = ?", whereArgs: [customer.id]);
  }

  Future<int> updateCustomer1(Customer customer) async {
    final db = await database; //??

    return await db.rawUpdate(
        'UPDATE Customer SET first_name = ${customer.firstName} WHERE id = ${customer.id}');
  }

  Future<int> deleteCustomer(int id) async {
    final db = await database; //??

    return await db.delete("Customer", where: 'id = ?', whereArgs: [id]);
  }

  Future<int> deleteCustomer1(int id) async {
    final db = await database; //??

    return await db.rawDelete('DELETE FROM Customer WHERE id = $id');
  }

  void close() async {
    final db = await database; //??

    await db.close();
  }

  // Fetch Operation: Get all custmer objects from database
  Future<List<Map<String, dynamic>>> getCustomerMapList() async {
    Database db = await this.database;

    var result = await db.rawQuery('SELECT * FROM $Customer');
    //	var result = await db.query(Customer_table init ?, orderBy: '$colPriority ASC');
    return result;
  }

  // Get the 'Map List' [ List<Map> ] and convert it to 'Note List' [ List<Note> ]
  Future<List<Customer>> getCustomerList() async {
    var noteMapList =
        await getCustomerMapList(); // Get 'Map List' from database
    int count =
        noteMapList.length; // Count the number of map entries in db table

    List<Customer> noteList = List<Customer>();
    // For loop to create a 'Note List' from a 'Map List'
    for (int i = 0; i < count; i++) {
      noteList.add(Customer.fromJson(noteMapList[i]));
    }
    return noteList;
  }
}
